#------------------------------------------------------------------------------
#                  GEOS-Chem Global Chemical Transport Model                  !
#------------------------------------------------------------------------------
#BOP
#
# !INCLUDE: UnitTest.2x25
#
# !DESCRIPTION: Input file that specifies debugging options for the 
#  GEOS-Chem unit tester.  Customized for 2x2.5 simulations.
#\\
#\\
# !REMARKS:  
#  To omit individual unit tests (or input settings), place a # comment
#  character in the first column.
#
#  For a complete explanation of how to customize the settings below for
#  your installation, please see these wiki posts:
#
#    http://wiki.geos-chem.org/GEOS-Chem_Unit_Tester#The_INPUTS_section
#    http://wiki.geos-chem.org/GEOS-Chem_Unit_Tester#The_RUNS_section
#
# !REVISION HISTORY: 
# Type 'gitk' at the prompt to browse the revision history.
#EOP
#------------------------------------------------------------------------------
#
# !INPUTS:
#
# %%% ID tags %%%
#
   VERSION          : v11-01
   DESCRIPTION      : Tests GEOS-Chem v11-01
#
# %%% Data path and HEMCO settings %%%
#
   DATA_ROOT        : /n/holylfs/EXTERNAL_REPOS/GEOS-CHEM/gcgrid/data/ExtData
   HEMCO_ROOT       : {DATAROOT}/HEMCO
   VERBOSE          : 3
   WARNINGS         : 3
#
# %%% Code, compiler, and queue settings %%%
#
   CODE_DIR         : {HOME}/GC/Code.v11-01
   COMPILER         : ifort
   MAKE_CMD         : make -j4 BOUNDS=y DEBUG=y FPE=y NO_ISO=y
   SUBMIT           : sbatch
#
# %%% Unit tester path names %%%
#
   UNIT_TEST_ROOT   : {HOME}/UT
   RUN_ROOT         : {UTROOT}/runs
   RUN_DIR          : {RUNROOT}/{RUNDIR}
   JOB_DIR          : {UTROOT}/jobs
   LOG_DIR          : {UTROOT}/logs/{VERSION}
   PERL_DIR         : {UTROOT}/perl
#
# %%% Web and text display options %%%
#
   TEMPLATE         : {PERLDIR}/ut_template.html
   TXT_GRID         : {LOGDIR}/{VERSION}.results.txt
   WEB_GRID         : {LOGDIR}/{VERSION}.results.html
   WEB_PUSH         : NONE
#
#  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#  %%%  OPTIONAL SLURM COMMANDS:                                    %%%
#  %%%                                                              %%%
#  %%%  Set these if your system uses the SLURM scheduler.          %%%
#  %%%  These will be used to set #SBATCH tags for the job script.  %%%
#  %%%  Otherwise you can comment these lines out.                  %%%
#  %%%                                                              %%%
#  %%%  NOTE: If you do use these SLURM commands, then also be      %%%
#  %%%  sure to set the SUBMIT command above to "sbatch".           %%%
#  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
   SLURM_CPUS       : 4
   SLURM_NODES      : 1
   SLURM_TIME       : 1-00:00
   SLURM_MEMORY     : 12000
   SLURM_PARTITION  : jacob
   SLURM_CONSTRAINT : intel
   SLURM_MAILTYPE   : ALL
   SLURM_MAILUSER   : your-email-address
   SLURM_STDOUT     : {LOGDIR}/{VERSION}.stdout.log
   SLURM_STDERR     : {LOGDIR}/{VERSION}.stderr.log
#
# !RUNS:
#  Specify the debugging runs that you want to perform below.
#  You can deactivate runs by commenting them out with "#".
#
#--------|-----------|------|------------|------------|--------------|--------|
# MET    | GRID      | NEST | SIMULATION | START DATE | END DATE     | EXTRA? |
#--------|-----------|------|------------|------------|--------------|--------|
# ======= Radon-Lead-Beryllium ===============================================
  geosfp   2x25        -      RnPbBe       2013070100   201307010020   -
  merra2   2x25        -      RnPbBe       2013070100   201307010020   -
  geos5    2x25        -      RnPbBe       2006070100   200607010020   -
# ======= Mercury ============================================================
  geosfp   2x25        -      Hg           2013010100   201301010020   -
  merra2   2x25        -      Hg           2013010100   201301010020   -
  merra    2x25        -      Hg           2009010100   200901010020   -
  geos5    2x25        -      Hg           2009010100   200901010020   -
# ======= POPs ===============================================================
  geosfp   2x25        -      POPs         2013070100   201307010020   -
  merra2   2x25        -      POPs         2013070100   201307010020   -
  geos5    2x25        -      POPs         2006070100   200607010020   -
# ======= Methane ============================================================
  geosfp   2x25        -      CH4          2013070100   201307010020   -
  merra2   2x25        -      CH4          2013070100   201307010020   -
  geos5    2x25        -      CH4          2006070100   200607010020   -
# ======= Tagged O3 ==========================================================
  geosfp   2x25        -      tagO3        2013070100   201307010020   -
  merra2   2x25        -      tagO3        2013070100   201307010020   -
  geos5    2x25        -      tagO3        2006070100   200607010020   -
# ======= Carbon Dioxide =====================================================
  geosfp   2x25        -      CO2          2013070100   201307010020   -
  merra2   2x25        -      CO2          2013070100   201307010020   -
  geos5    2x25        -      CO2          2006070100   200607010020   -
# ======= Offline Aerosols ===================================================
  geosfp   2x25        -      aerosol      2013070100   201307010020   -
  merra2   2x25        -      aerosol      2013070100   201307010020   -
  geos5    2x25        -      aerosol      2006070100   200607010020   -
# ======= GEOS-Chem benchmark ================================================
  geosfp   2x25        -      standard     2013070100   201307010020   -
# ======= Tropchem ===========================================================
  geosfp   2x25        -      tropchem     2013070100   201307010020   -
  merra2   2x25        -      tropchem     2013070100   201307010020   -
  geos5    2x25        -      tropchem     2006070100   200607010020   -
# ======= SOA (w/o SVPOA) ====================================================
  geosfp   2x25        -      soa          2013070100   201307010020   -
  merra2   2x25        -      soa          2013070100   201307010020   -
  geos5    2x25        -      soa          2006070100   200607010020   -
# ======= SOA (w/ SVPOA) =====================================================
  geosfp   2x25        -      soa_svpoa    2013070100   201307010020   -
  merra2   2x25        -      soa_svpoa    2013070100   201307010020   -
  geos5    2x25        -      soa_svpoa    2006070100   200607010020   -
# ======= Acid uptake on dust ================================================
  geosfp   2x25        -      aciduptake   2013070100   2013070101     -
# ======= Marine POA =========================================================
  geosfp   2x25        -      marinePOA    2013070100   2013070101     -
# ======= UCX strat-trop chemistry ===========================================
  geosfp   2x25        -      UCX          2013070100   201307010020   -
  merra2   2x25        -      UCX          2013070100   201307010020   -
  geos5    2x25        -      UCX          2006070100   200607010020   -
# ======= RRTMG online radiative transfer ====================================
# geosfp   2x25        -      RRTMG        2013070100   201307010040   -
# merra2   2x25        -      RRTMG        2013070100   201307010040   -
# geos5    2x25        -      RRTMG        2006070100   200607010040   -
 !END OF RUNS:
#EOP
#------------------------------------------------------------------------------
