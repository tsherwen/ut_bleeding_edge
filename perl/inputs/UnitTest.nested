#------------------------------------------------------------------------------
#                  GEOS-Chem Global Chemical Transport Model                  !
#------------------------------------------------------------------------------
#BOP
#
# !INCLUDE: UnitTest.nested
#
# !DESCRIPTION: Input file that specifies debugging options for the 
#  GEOS-Chem unit tester.  Customized for nested-grid simulations.
#\\
#\\
# !REMARKS:  
#  To omit individual unit tests (or input settings), place a # comment
#  character in the first column.
#
#  For a complete explanation of how to customize the settings below for
#  your installation, please see these wiki posts:
#
#    http://wiki.geos-chem.org/GEOS-Chem_Unit_Tester#The_INPUTS_section
#    http://wiki.geos-chem.org/GEOS-Chem_Unit_Tester#The_RUNS_section
#
# !REVISION HISTORY: 
# Type 'gitk' at the prompt to browse the revision history.
#EOP
#------------------------------------------------------------------------------
#
# !INPUTS:
#
# %%% ID tags %%%
#
   VERSION          : v11-01
   DESCRIPTION      : Tests GEOS-Chem v11-01
#
# %%% Data path and HEMCO settings %%%
#
   DATA_ROOT        : /n/holylfs/EXTERNAL_REPOS/GEOS-CHEM/gcgrid/data/ExtData
   HEMCO_ROOT       : {DATAROOT}/HEMCO
   VERBOSE          : 3
   WARNINGS         : 3
#
# %%% Code, compiler, and queue settings %%%
#
   CODE_DIR         : {HOME}/GC/Code.v11-01
   COMPILER         : ifort
   MAKE_CMD         : make -j4 BOUNDS=y DEBUG=y FPE=y NO_ISO=y
   SUBMIT           : sbatch
#
# %%% Unit tester path names %%%
#
   UNIT_TEST_ROOT   : {HOME}/UT
   RUN_ROOT         : {UTROOT}/runs
   RUN_DIR          : {RUNROOT}/{RUNDIR}
   JOB_DIR          : {UTROOT}/jobs
   LOG_DIR          : {UTROOT}/logs/{VERSION}
   PERL_DIR         : {UTROOT}/perl
#
# %%% Web and text display options %%%
#
   TEMPLATE         : {PERLDIR}/ut_template.html
   TXT_GRID         : {LOGDIR}/{VERSION}.results.txt
   WEB_GRID         : {LOGDIR}/{VERSION}.results.html
   WEB_PUSH         : NONE
#
#  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#  %%%  OPTIONAL SLURM COMMANDS:                                    %%%
#  %%%                                                              %%%
#  %%%  Set these if your system uses the SLURM scheduler.          %%%
#  %%%  These will be used to set #SBATCH tags for the job script.  %%%
#  %%%  Otherwise you can comment these lines out.                  %%%
#  %%%                                                              %%%
#  %%%  NOTE: If you do use these SLURM commands, then also be      %%%
#  %%%  sure to set the SUBMIT command above to "sbatch".           %%%
#  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
   SLURM_CPUS       : 4
   SLURM_NODES      : 1
   SLURM_TIME       : 1-00:00
   SLURM_MEMORY     : 12000
   SLURM_PARTITION  : jacob
   SLURM_CONSTRAINT : intel
   SLURM_MAILTYPE   : ALL
   SLURM_MAILUSER   : your-email-address
   SLURM_STDOUT     : {LOGDIR}/{VERSION}.stdout.log
   SLURM_STDERR     : {LOGDIR}/{VERSION}.stderr.log
#
# !RUNS:
#  Specify the debugging runs that you want to perform below.
#  You can deactivate runs by commenting them out with "#".
#
#--------|-----------|------|------------|------------|--------------|--------|
# MET    | GRID      | NEST | SIMULATION | START DATE | END DATE     | EXTRA? |
#--------|-----------|------|------------|------------|--------------|--------|
  geosfp   025x03125   ch     tropchem     2013070100   201307010010   -
  geosfp   025x03125   na     tropchem     2013070100   201307010010   -
  merra2   05x0625     as     tropchem     2013070100   201307010020   -
  merra2   05x0625     na     tropchem     2013070100   201307010020   -
  geos5    05x0666     ch     tropchem     2006070100   200607010020   -
  geos5    05x0666     na     tropchem     2006070100   200607010020   -
 !END OF RUNS:
#EOP
#------------------------------------------------------------------------------
